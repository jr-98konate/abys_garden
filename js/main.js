(function ($) {
    "use strict";

    // Spinner
    var spinner = function () {
        setTimeout(function () {
            if ($('#spinner').length > 0) {
                $('#spinner').removeClass('show');
            }
        }, 1);
    };
    spinner();
    
    
    // Initiate the wowjs
    new WOW().init();


    // Sticky Navbar
    $(window).scroll(function () {
        if ($(this).scrollTop() > 45) {
            $('.navbar').addClass('sticky-top shadow-sm');
        } else {
            $('.navbar').removeClass('sticky-top shadow-sm');
        }
    });
    
    
    // Dropdown on mouse hover
    const $dropdown = $(".dropdown");
    const $dropdownToggle = $(".dropdown-toggle");
    const $dropdownMenu = $(".dropdown-menu");
    const showClass = "show";
    
    $(window).on("load resize", function() {
        if (this.matchMedia("(min-width: 992px)").matches) {
            $dropdown.hover(
            function() {
                const $this = $(this);
                $this.addClass(showClass);
                $this.find($dropdownToggle).attr("aria-expanded", "true");
                $this.find($dropdownMenu).addClass(showClass);
            },
            function() {
                const $this = $(this);
                $this.removeClass(showClass);
                $this.find($dropdownToggle).attr("aria-expanded", "false");
                $this.find($dropdownMenu).removeClass(showClass);
            }
            );
        } else {
            $dropdown.off("mouseenter mouseleave");
        }
    });
    
    
    // Back to top button
    $(window).scroll(function () {
        if ($(this).scrollTop() > 300) {
            $('.back-to-top').fadeIn('slow');
        } else {
            $('.back-to-top').fadeOut('slow');
        }
    });
    $('.back-to-top').click(function () {
        $('html, body').animate({scrollTop: 0}, 1500, 'easeInOutExpo');
        return false;
    });


    // Facts counter
    $('[data-toggle="counter-up"]').counterUp({
        delay: 10,
        time: 2000
    });


    // Modal Video
    $(document).ready(function () {
        var $videoSrc;
        $('.btn-play').click(function () {
            $videoSrc = $(this).data("src");
        });
        console.log($videoSrc);

        $('#videoModal').on('shown.bs.modal', function (e) {
            $("#video").attr('src', $videoSrc + "?autoplay=1&amp;modestbranding=1&amp;showinfo=0");
        })

        $('#videoModal').on('hide.bs.modal', function (e) {
            $("#video").attr('src', $videoSrc);
        })
    });


    // Testimonials carousel
    $(".testimonial-carousel").owlCarousel({
        autoplay: true,
        smartSpeed: 1000,
        center: true,
        margin: 24,
        dots: true,
        loop: true,
        nav : false,
        responsive: {
            0:{
                items:1
            },
            768:{
                items:2
            },
            992:{
                items:3
            }
        }
    });
    
})(jQuery);

// partainer
function logo_carouselInit() {
    $('.owl-carousel.logo_active').owlCarousel({
        dots: false,
        loop: true,
        margin: 30,
        stagePadding: 2,
        smartSpeed: 1000,
        autoplay: true,
        autoplayTimeout: 1500,
        autoplayHoverPause: true,
        responsive: {
            0: {
                items: 2
            },
            576: {
                items: 3,
            },
            768: {
                items: 4,
            },
            992: {
                items: 5
            }
        }
    });
}

// SLIDE
var copy = document.querySelector(".logos-slide").cloneNode(true);
document.querySelector(".logos").appendChild(copy);

// ==================reservation===============================


function sendWhatsAppMessage(event) {
    event.preventDefault(); 

    // Récupérer les données du formulaire
    const nom = document.getElementById('nom').value;
    const telephone = document.getElementById('telephone').value;
    const dateTime = document.getElementById('dateTime').value;
    const heureElement = document.getElementById('heure');

    // Vérifier que l'élément heure existe et récupérer sa valeur
    const heure = heureElement ? heureElement.value : '';

    

    const nombrePersonne = document.getElementById('nombrePersonne').value;

    // Construire le message
    const message = `Je voudrais réserver une table:
Nom: ${nom}
Téléphone: ${telephone}
Date: ${dateTime}
Heure: ${heure}
Nombre de personnes: ${nombrePersonne}`;

    // Encode le message pour l'URL
    const encodedMessage = encodeURIComponent(message);

    // Numéro de téléphone WhatsApp
    const phoneNumber = '+766249918';

    // Créer le lien WhatsApp
    const whatsappLink = `https://wa.me/${phoneNumber}?text=${encodedMessage}`;

    // Ouvrir le lien WhatsApp dans une nouvelle fenêtre
    window.open(whatsappLink, '_blank');

    // Réinitialiser le formulaire
    document.getElementById('reservationForm').reset();
}

// TEXT

const text = document.querySelector(".sec-text");
const  textLoad =() =>{
    setTimeout(() =>{
        text.textContent = "à Aby's Garden";
    }, 0)

    setTimeout(() =>{
        text.textContent  = " au Meilleur restaurant";
    }, 4000);
    setTimeout(()=>{
        text.textContent = "à Dakar";
    }, 8000)
}
textLoad();
setInterval
(textLoad, 12000)

// message contant



function sendMessageToWhatsApp() {
    // Récupérer les valeurs du formulaire
    const name = document.getElementById('name').value.trim();
    const email = document.getElementById('email').value.trim();
    const subject = document.getElementById('subject').value.trim();
    const message = document.getElementById('message').value.trim();

    // Vérifier si tous les champs sont remplis
    if (!name || !email || !subject || !message) {
        alert("Veuillez remplir tous les champs avant d'envoyer un message via WhatsApp.");
        return;
    }

    // Construire l'URL du message WhatsApp
    const phoneNumber = "+766249918"; // remplacez par votre numéro WhatsApp
    const info = "Bonjour j'aimerais obtenir des information s'il vous plaît:"
    const text = `${info}%0A%0ANom: ${name}%0AEmail: ${email}%0ASujet: ${subject}%0AMessage: ${message}`;
    const url = `https://api.whatsapp.com/send?phone=${phoneNumber}&text=${text}`;

    // Ouvrir WhatsApp avec le message
    window.open(url, '_blank');
    document.getElementById("messageInfo").reset()
}






